import sys
import subprocess


if __name__ == '__main__':
    try:
        file_name = sys.argv[1]

        # call spamassassin with the given file and return the points
        output = subprocess.check_output("spamassassin --test-mode < " + file_name, shell=True, stderr=subprocess.STDOUT)
        if output:
            lines = output.split('\n')
            if lines and lines[-4]:
                results = lines[-4].split(' ')
                if results and len(results)>0:
                    points = results[0]
                    print "%d" % round(float(points))
                    exit()
        print "0"
    except Exception as e:
        print "ERROR"


